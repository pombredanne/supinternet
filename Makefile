LATEX := pdflatex
TARGETS := https.pdf csrf.pdf xss.pdf sop.pdf questions.pdf

.PHONY: all clean distclean

all: $(TARGETS) clean

%.pdf: %.tex
	$(LATEX) --shell-escape $<
	rm -f $@
	$(LATEX) --shell-escape $<

clean:
	rm -f *.log *.nav *.snm *.aux *.out *.toc *.pyg

distclean: clean
	rm -f $(TARGETS)
