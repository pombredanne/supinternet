<?php require 'vendor/autoload.php';

session_start();

/* perform login process */
function login($name, $pwd) {
    /* login to database and sanitize input */
    $link = mysqli_connect('localhost', 'root', '', 'xss');
    $name = mysqli_real_escape_string($link, $name);
    $pwd = mysqli_real_escape_string($link, $pwd);

    /* fetch password matching this name */
    $res = mysqli_query($link, "SELECT pwd FROM users WHERE name='$name'");
    if (!$res)
        printf("DB Error: %s\n", mysqli_error($link));
    $row = mysqli_fetch_array($res);

    /* if name exists and password matches the one given in the form... */
    if ($row['pwd'] === $_POST['pwd']) {
        $sid = session_id();
        $_SESSION['loggedin'] = true;

        /* check if session id is already registered in database */
        $res = mysqli_query($link, "SELECT sid FROM sessions WHERE sid='$sid'");
        if (!$res)
            printf("DB Error: %s\n", mysqli_error($link));
        $row = mysqli_fetch_array($res);
        if ($row['sid'] !== NULL) {
            mysqli_close($link);
            return;
        }

        /* if session id doesn't exist already, add it */
        $res = mysqli_query($link, "INSERT INTO sessions VALUES('$sid', '$name')");
        if (!$res)
            printf("DB Error: %s\n", mysqli_error($link));
    }

    mysqli_close($link);
}

/* checks if the current session is logged in and considered valid */
function is_loggedin() {
    $sid = session_id();

    /* if session is marked as logged in, check */
    if (isset($_SESSION['loggedin']) && $_SESSION['loggedin']) {
        $link = mysqli_connect('localhost', 'root', '', 'xss');

        /* try to fetch the session id in the db */
        $res = mysqli_query($link, "SELECT sid FROM sessions WHERE sid='$sid'");
        if (!$res)
            printf("DB Error: %s\n", mysqli_error($link));
        $row = mysqli_fetch_array($res);
        mysqli_close($link);

        /* if row is not null, then the user is logged in */
        return ($row !== NULL);
    }

    /* session is not logged in */
    return false;
}

function fetch_name() {
    /* fetch the row matching the current session id */
    $sid = session_id();
    $link = mysqli_connect('localhost', 'root', '', 'xss');
    $res = mysqli_query($link, "SELECT * FROM sessions WHERE sid='$sid'");
    if (!$res)
        printf("DB Error: %s\n", mysqli_error($link));
    mysqli_close($link);

    /* if the row is empty, user is not logged in: there is something wrong */
    $row = mysqli_fetch_array($res);
    if ($row === NULL)
        return 'visitor';

    return $row['name'];
}

$app = new \Slim\Slim();

$app->get('/', function() {
?>
    <h1>Welcome to the site!</h1>

    <p>You can view comments <a href='/comments'>here</a> and take a look at
    the gallery <a href='/images'>here</a>.</p>

<?php

    if (is_loggedin()) {
?>
    <p><a href='/logout'>Logout!</a></p>
<?php
    }
    else {
?>
    <p><a href='/login'>Admin panel login. Please ignore!</a></p>
<?php
    }
});

$app->get('/login', function() {
    if (is_loggedin()) {
?>
    <p>You are already logged in!</p>
<?php
    }
    else {
?>
    <form action='/login' method='post'>
        <input name='name' type='text' />
        <input name='pwd' type='password' />
        <input type='submit' value='login' />
    </form>
<?php
    }
});

$app->post('/login', function() use ($app) {
    if (isset($_POST['name']) && isset($_POST['pwd'])) {
        login($_POST['name'], $_POST['pwd']);
        $app->redirect('/');
    }
    else {
        /* if post values do not exist... something is wrooooooooong */
?>
        <p>Invalid login form submitted!</p>
<?php
    }
});

// see all comments
$app->get('/comments', function() {
?>
    <table border="2">
    <tr>
    <th>name</th>
    <th>message</th>
    </tr>
<?php
    /* select all comments */
    $link = mysqli_connect('localhost', 'root', '', 'xss');
    $res = mysqli_query($link, "SELECT * FROM comments");
    if (!$res)
        printf("DB Error: %s\n", mysqli_error($link));
    mysqli_close($link);

    /* print comments out */
    $row = mysqli_fetch_array($res);
    while ($row !== NULL) {
        /* sanitize input!!! */
        $name = htmlspecialchars($row['name']);
        $msg = htmlspecialchars($row['message']);

?>
        <tr>
        <td><?php echo $name; ?></td>
        <td><?php echo $msg; ?></td>
        </tr>
<?php
        /* go on to the next entry */
        $row = mysqli_fetch_array($res);
    }

?>
    </table>

    <p>Post your own comment!</p>
    <table>
    <form action='/comments' method='post'>
    <tr>
        <td><input type='text' name='name' value='<?php echo fetch_name(); ?>' /></td>
        <td><input type='text' name='message' value='Your message' /></td>
        <td><input type='submit' value='send' /></td>
    </tr>
    </form>
    <table>

    <p>go back to <a href='/'>main page</a></p>
<?php
});

// post the incoming comment and display all comments
$app->post('/comments', function() {
    if (isset($_POST['name']) && isset($_POST['message'])) {
        /* connect to db and sanitize input */
        $link = mysqli_connect('localhost', 'root', '', 'xss');
        $name = mysqli_real_escape_string($link, htmlspecialchars($_POST['name']));
        $msg = mysqli_real_escape_string($link, htmlspecialchars($_POST['message']));

        /* actually post comment */
        $res = mysqli_query($link, "INSERT INTO comments VALUES('$name', '$msg')");
        if (!$res)
            printf("DB Error: %s\n", mysqli_error($link));
        mysqli_close($link);

?>
        <p>comment posted! <a href="/comments">reload page</a></p>
<?php
    }
    else {
?>
        <p>Invalid comment form submitted!</p>
<?php
    }

});

// just display images and propose to upload an image
$app->get('/images', function() {
    /* connect to db and retrieve images descriptors */
    $link = mysqli_connect('localhost', 'root', '', 'xss');
    $res = mysqli_query($link, "SELECT * FROM images");
    if (!$res)
        printf("DB Error: %s\n", mysqli_error($link));

    /* iterate over image descriptors, displaying the matching image each time */
    $row = mysqli_fetch_array($res);
    while ($row !== NULL) {
?>
        <h2><?php echo $row['title'] . ' by ' . $row['name']; ?></h2>
        <a href="<?php echo $row['path']; ?>">
            <img src="<?php echo $row['path']; ?>" />
        </a>
        <hr />
<?php
        $row = mysqli_fetch_array($res);
    }

?>
    <h1>Post an image yourself!</h1>
    <form action='/images' method='post' enctype='multipart/form-data'>
        <input type='text' name='name' value='<?php echo fetch_name(); ?>'/>
        <input type='text' name='title' value='image title' />
        <input type='file' name='file' />
        <input type='submit' value='ok' />
    </form>

    <p>go back to <a href='/'>main page</a></p>
<?php
});

// add image to database
$app->post('/images', function() use ($app) {
    /* sanitize input and connect to db */
    $link = mysqli_connect('localhost', 'root', '', 'xss');
    $path = mysqli_real_escape_string($link, htmlspecialchars($_FILES['file']['name']));
    $title = mysqli_real_escape_string($link, htmlspecialchars($_POST['title']));
    $name = mysqli_real_escape_string($link, htmlspecialchars($_POST['name']));

    /* add name and path to image in the database */
    $res = mysqli_query($link, "INSERT INTO images VALUES('$title', '$path', '$name')");
    if (!$res)
        printf("DB Error: %s\n", mysqli_error($link));

    /* move file into current directory */
    move_uploaded_file($_FILES['file']['tmp_name'], $path);

?>
    <p>Image correctly added to the gallery! Thanks for your input!</p>
<?php

    $app->redirect('/images');
});

$app->get('/logout', function() use ($app) {
    /* remove session id from db */
    $sid = session_id();
    $link = mysqli_connect('localhost', 'root', '', 'xss');
    $res = mysqli_query($link, "DELETE FROM sessions WHERE sid='$sid'");
        if (!$res)
            printf("DB Error: %s\n", mysqli_error($link));
    mysqli_close($link);

    /* destroy current session */
    session_destroy();
    $app->redirect('/');
});

$app->run();

?>
